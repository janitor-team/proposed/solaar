Source: solaar
Maintainer: Stephen Kitt <skitt@debian.org>
Section: misc
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: dh-python,
                     gir1.2-gtk-3.0,
                     po-debconf,
                     python3,
                     python3-gi,
                     python3-pyudev,
                     python3-setuptools
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/debian/solaar
Vcs-Git: https://salsa.debian.org/debian/solaar.git
Homepage: https://pwr-solaar.github.io/Solaar/
Rules-Requires-Root: no

Package: solaar
Architecture: all
Depends: ${misc:Depends},
         ${debconf:Depends},
         udev,
         passwd | adduser,
         ${python3:Depends},
         gir1.2-notify-0.7,
         gir1.2-ayatanaappindicator3-0.1,
Recommends: python3-dbus,
            upower
Description: Logitech Unifying Receiver peripherals manager for Linux
 Solaar is a Linux device manager for Logitech's Unifying Receiver wireless
 peripherals. It is able to pair/unpair devices to the receiver, and for
 some devices to read battery status.

Package: solaar-gnome3
Architecture: all
Section: oldlibs
Depends: ${misc:Depends}, solaar (= ${source:Version})
Description: GNOME Shell integration for Solaar (transitional package)
 Solaar is a Linux device manager for Logitech's Unifying Receiver wireless
 peripherals. It is able to pair/unpair devices to the receiver, and for
 some devices to read battery status.
 .
 This metapackage ensures integration with GNOME Shell/Unity. It is no
 longer needed and can safely be uninstalled.
